// variables
const email = document.getElementById('email');
const asunto = document.getElementById('asunto');
const mensaje = document.getElementById('mensaje');
const btnEnviar = document.getElementById('enviar');
const formularioEnviar = document.getElementById('enviar-mail');
const resetBtn = document.getElementById('resetBtn');

// event listener
eventListeners();

function eventListeners () {
    // inicio de la aplicacion y deshabilitar submit
    document.addEventListener('DOMContentLoaded', inicioApp);

    // campos del formulario
    email.addEventListener('blur', validarCampo);
    asunto.addEventListener('blur', validarCampo);
    mensaje.addEventListener('blur', validarCampo);

    // boton de enviar en el submit
    formularioEnviar.addEventListener('submit', enviarEmail);

    // boton de reset
    resetBtn.addEventListener('click', resetFormulario);
}

// funciones

// deshabilitar el envio
function inicioApp () {
    btnEnviar.disabled = true;
}

// valida que el campo tenga algo escrito
function validarCampo () {
    // se valida la longitud del texto y que no este vacio
    validarLongitud(this);

    // validar unicamente el email
    if (this.type === 'email') {
        validarEmail(this);
    }

    let errores = document.querySelectorAll('.error');

    if (email.value !== '' && asunto.value !== '' && mensaje.value !== '') {
        if (errores.length === 0) {
            btnEnviar.disabled = false;
        }
    }
}

// cuando se envia el correo
function enviarEmail (e) {
    // spinner al presionar enviar
    const spinnerGif = document.querySelector('#spinner');
    spinnerGif.style.display = 'block';

    // gif que envia el email
    const enviado = document.createElement('img');
    enviado.src = 'img/mail.gif';
    enviado.style.display = 'block';

    // ocultar spinner y mostrar gif de enviado
    setTimeout(() => {
        spinnerGif.style.display = 'none';

        document.querySelector('#loaders').appendChild(enviado);

        setTimeout(() => {
            enviado.remove();
            formularioEnviar.reset();
        }, 5000);
    }, 2000);

    e.preventDefault();

        
}

// verifica la longitud del texto en los campos
function validarLongitud (campo) {
    if (campo.value.length > 0) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
    } else {
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
    }
}

// validar que el email tenga un @ en su contenido
function validarEmail (campo) {
    const mensaje = campo.value;

    if (mensaje.indexOf('@') !== -1) {
        campo.style.borderBottomColor = 'green';
        campo.classList.remove('error');
    } else {
        campo.style.borderBottomColor = 'red';
        campo.classList.add('error');
    }
}

// resetear el formulario
function resetFormulario (e) {
    formularioEnviar.reset();
    e.preventDefault();
}